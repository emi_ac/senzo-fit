<?php
/*
* Template Name: Contacto
*
* This is the template that displays the front page.
* 
* @link https://developer.wordpress.org/themes/functionality/custom-front-page-templates/
*
* @package senzofit
*/
get_header();
?>	
<div class="pagina-contacto pb-5">
    <div class="pagina-contacto__banner">
		<img class="img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/contacto-banner.jpg'?> " alt="">
	</div>
	<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="pagina-contacto__formulario">
                    <h1 class="pagina-contacto__titulo">CONTACTO</h1>
                    <p class="pagina-contacto__bajada">Si tienes alguna duda o consulta escribenos
                        y te responderemos a la brevedad.</p>
                    </p>
                    <?php echo do_shortcode( '[contact-form-7 id="238" title="Formulario de contacto 1"]' ); ?>
                </div>
            </div>
        </div>
	</div>
</div>
<?php
get_footer();