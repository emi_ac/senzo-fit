<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package senzofit
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#222222">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'senzofit' ); ?></a>

	<header id="masthead" class="site-header sticky-top">
		<nav class="navegacion navbar navbar-expand-xl">
			<div class="container">
				<div class="navegacion__wrap">
					<button class="navegacion__toggler navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
						<span class="navbar-toggler-icon">
							<i class='bx bx-menu-alt-left'></i>
						</span>
					</button>
					<div class="navegacion__logotipo">
						<?php the_custom_logo(); ?>
					</div>
					<div class="navegacion-carrito-container d-flex d-xl-none">
						<a class="navegacion__carrito"></a>
					</div>
				</div>
				<div class="navegacion__buscador">
					<?php aws_get_search_form( true ); ?>
				</div>
				<div class="collapse navbar-collapse justify-content-end pe-4 align-items-center" id="navbarSupportedContent">
					<div class="navegacion__menu">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'menu-1',
									'menu_id'        => 'primary-menu',
								)
							);
						?>
					</div>
					<a class="navegacion__carrito"></a>
				</div>
			</div>
		</nav>

		<div class="navegacion-movil offcanvas offcanvas-start" 
			 tabindex="-1" 
			 id="offcanvasExample" 
			 aria-labelledby="offcanvasExampleLabel">
			<div class="offcanvas-header justify-content-end py-0">
				<button 
					type="button" 
					class="boton-cerrar btn btn-link p-0" 
					data-bs-dismiss="offcanvas" 
					aria-label="Close">
					<i class='bx bx-x'></i>
				</button>
			</div>
			<div class="offcanvas-body">
				<?php the_custom_logo(); ?>			
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						)
					);
				?>
				<div class="redes-sociales d-flex">
					<a href="#"><i class='bx bxl-facebook'></i></a>
					<a href="#"><i class='bx bxl-instagram'></i></a>
					<a href="#"><i class='bx bxs-envelope'></i></a>		
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
