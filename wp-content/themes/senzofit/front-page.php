<?php
/**
 * The custom site front page template
 *
 * This is the template that displays the front page.
 * 
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-front-page-templates/
 *
 * @package senzofit
 */

get_header();
?>
	<main id="primary" class="site-main">
        <section class="slider-header">
            <?php 
                if (shortcode_exists('smartslider3')) {
                    echo do_shortcode('[smartslider3 slider="2"]');
                }
            ?>         
        </section>

        <section class="seccion-categorias">
            <div class="container">
                <h2 class="titulo-seccion">Categorías</h2>
                <div class="swiper-container slider-categorias pt-2 pb-5">
                    <div class="swiper-wrapper">   
                        <div class="swiper-slide">
                            <article class="categoria">
                                <a href="<?php echo get_term_link( 'musculacion', 'product_cat' ); ?>">
                                    <div class="categoria__img">
                                        <img src="<?php echo get_theme_mod('set_categorias_productos_musculacion'); ?>" alt="Miniatura categoria">
                                    </div>
                                    <h3 class="categoria__titulo">Musculación</h3>
                                    <div class="linea"></div>
                                </a>
                            </article>
                        </div> 
                        <div class="swiper-slide">
                            <article class="categoria">
                                <a href="<?php echo get_term_link( 'home-gym', 'product_cat' ); ?>">
                                    <div class="categoria__img">
                                        <img src="<?php echo get_theme_mod('set_categorias_productos_fitness'); ?>" alt="Miniatura categoria">
                                    </div>
                                    <h3 class="categoria__titulo">Home GYM</h3>
                                    <div class="linea"></div>
                                </a>
                            </article>
                        </div> 
                        <div class="swiper-slide">
                            <article class="categoria">
                                <a href="<?php echo get_term_link( 'fitness', 'product_cat' ); ?>">
                                    <div class="categoria__img">
                                        <img src="<?php echo get_theme_mod('set_categorias_productos_home-gym'); ?>" alt="Miniatura categoria">
                                    </div>
                                    <h3 class="categoria__titulo">Fitness</h3>
                                    <div class="linea"></div>
                                </a>
                            </article>
                        </div> 
                        <div class="swiper-slide">
                            <article class="categoria">
                                <a href="<?php echo get_term_link( 'accesorios', 'product_cat' ); ?>">
                                    <div class="categoria__img">
                                        <img src="<?php echo get_theme_mod('set_categorias_productos_accesorios'); ?>" alt="">
                                    </div>
                                    <h3 class="categoria__titulo">Accesorios</h3>
                                    <div class="linea"></div>
                                </a>
                            </article>
                        </div> 
                    </div>
                    <div class="swiper-pagination categorias-paginacion"></div>
                </div>   
            </div>
        </section>

        <section class="seccion-productos-destacados mt-5">
            <div class="container">
                <?php
                    // The tax query
                    $tax_query[] = array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'featured',
                        'operator' => 'IN', 
                    );

                    // The query
                    $query = new WP_Query( array(
                        'post_type'           => 'product',
                        'post_status'         => 'publish',
                        'ignore_sticky_posts' => 2,
                        'posts_per_page'      => 4,
                        'orderby'             => 'rand',
                        'order'               => $order == 'asc' ? 'asc' : 'desc',
                        'tax_query'           => $tax_query
                    ) );
        
                    if ( $query->have_posts() ) {
                        echo '<h2 class="titulo-seccion">Destacados</h2>';
                        echo '<div class="woocommerce columns-4">';
                            echo '<ul class="products columns-4">';
                                while ( $query->have_posts() ) : $query->the_post();
                                    wc_get_template_part( 'content', 'product' );
                                endwhile;
                            echo '</ul>';
                        echo '</div>';
                    } else {
            
                    }
                    wp_reset_postdata();
                ?>
            </div>
        </section>

        <section class="seccion-sobre-nosotros">
            <img class="seccion-sobre-nosotros__img-izquierda" src="<?php echo get_theme_mod('set_producto_destacado'); ?>" />
            <div class="seccion-sobre-nosotros__texto">
                <h2> <?php echo get_theme_mod('set_producto_destacado_titulo') ?></h2>
                <p>  <?php echo get_theme_mod('set_producto_destacado_texto'); ?></p>
            </div>
            <img class="seccion-sobre-nosotros__img-derecha" src="<?php echo get_theme_mod('set_producto_destacado_imagen_derecha'); ?>" />
        </section>

        <section class="seccion-novedades">
            <div class="container">
                <h2 class="titulo-seccion">Novedades</h2>
                <?php echo do_shortcode('[products columns="4" limit="8" orderby="date" ]');  ?>
                <p class="text-center">
                    <a class="btn ver-mas-productos mt-5" href=" <?php echo get_permalink( wc_get_page_id( 'shop' ));?> ">Ver más productos </a>
                </p>
            </div>
        </section>

        <section class="seccion-newsletter" style=" background-image:url('<?php echo get_template_directory_uri() ;?>/assets/img/footer-bg.jpg)'">
            <div class="container">
                <div class="seccion-newsletter__contenido">
                    <h3 class="seccion-newsletter__titulo">Subscribete a nuestro newsletter</h3>
                    <p class="seccion-newsletter__bajada">Recibe las mejores ofertas antes que nadie</p>
                    <?php 
                        if( shortcode_exists( 'yikes-mailchimp')) {
                            echo do_shortcode('[yikes-mailchimp form="1"]'); 
                        }
                    ?>
                </div>
            </div>
        </section>
        
        <script>
            document.addEventListener('DOMContentLoaded', function () {

                var controller = new ScrollMagic.Controller();

                var escena1 = new ScrollMagic.Scene({
                    triggerElement: '.seccion-productos-destacados',
                    triggerHook: 1,    
                })
                .setClassToggle('.seccion-productos-destacados', 'fade-in-senzo') 
                .addTo(controller);

                var escena2 = new ScrollMagic.Scene({
                    triggerElement: '.seccion-sobre-nosotros',   
                })
                .setClassToggle('.seccion-sobre-nosotros', 'fade-in-senzo') 
                .addTo(controller);

                var escena3 = new ScrollMagic.Scene({
                    triggerElement: '.seccion-novedades',
                    triggerHook: 0.9,    
                })
                .setClassToggle('.seccion-novedades', 'fade-in-senzo') 
                .addTo(controller);
            });
        </script>
	</main><!-- #main -->
<?php
get_footer();
