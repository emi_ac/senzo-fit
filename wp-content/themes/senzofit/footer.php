<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package senzofit
 */

?>
	<footer id="colophon" class="site-footer seccion-footer py-5">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-xl-3 col-lg-6 d-flex justify-content-center justify-content-lg-start">
					<div class="footer__logo">
						<?php the_custom_logo(); ?>
					</div>
				</div>
				<div class="col-xl-6 col-lg-6">
					<div class="row align-items-center">
						<div class="col-lg-6 d-flex d-lg-block justify-content-center my-4 my-lg-0 mb-lg-0">
							<?php
								wp_nav_menu(
									array(
										'theme_location' => 'footer_menu',
										'menu_class'     => 'footer-menu',
										'container'      => 'ul',
									)
								);
							?>
						</div>
						<div class="col-lg-6 d-flex justify-content-center justify-content-lg-end">
							<div class="redes-sociales">
								<a href="#"><i class='bx bxl-facebook'></i></a>
								<a href="#"><i class='bx bxl-instagram'></i></a>
								<a href="#"><i class='bx bxs-envelope'></i></a>		
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-12 mt-5 mt-lg-0 d-flex justify-content-center justify-content-lg-end">
					<img class="footer__medio-pago" class="img-fluid" src="<?php echo get_template_directory_uri() ;?>/assets/img/webpay.png" alt="Medios de pago">
				</div>
			</div>
		</div>
	</footer>
	<div class="copyright">
		© 2021 Senzofit todos los derechos reservados.
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
