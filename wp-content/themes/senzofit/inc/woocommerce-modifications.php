<?php

function senzofit_wc_modifications() {

    /**
    * Modificaciones generales.
    */
    
        // Cierre y apertura container sitio.
        add_action( 'woocommerce_before_main_content', 'senzofit_open_container_row', 5);
        function senzofit_open_container_row() {
            echo '<div class="container yse"> <div class="row">';
        }
        add_action( 'woocommerce_after_main_content', 'senzofit_close_container_row', 10);
        function senzofit_close_container_row() {
            echo '</div></div>';
        }
        
        // Remover barra lateral de todas las paginas.
        remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar');


    /**
    * Modificaciones pagina producto
    */
        if(is_product()) {

            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt' ); 

            //  Añadir contenedor pagina producto
            add_action( 'woocommerce_before_main_content', 'senzofit_product_page_wrapper_open', 1);
            function senzofit_product_page_wrapper_open() {
                echo '<div class="pagina-producto">';
            }
            add_action( 'woocommerce_after_main_content', 'senzofit_product_page_wrapper_close', 12);
            function senzofit_product_page_wrapper_close() {
                echo '</div>';
            }

            // Remover SKU Y categorias
            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

            // Añadir SKU
            add_action( 'woocommerce_single_product_summary', 'senzofit_add_sku', 5 );
            function senzofit_add_sku() {
                global $product;
                if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
                    <span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> 
                        <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span>
                    </span>
                <?php endif;?>
                <?php
            }

            // Añadir categorias
            add_action( 'woocommerce_single_product_summary', 'senzofit_add_categories', 40 );
            function senzofit_add_categories() { 
                global $product;
                ?> 
                    <?php echo wc_get_product_category_list( $product->get_id(), '', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
                <?php
            }

            // Añadir medio de pago
            add_action( 'woocommerce_single_product_summary', 'senzofit_add_medio_de_pago', 39);
            function senzofit_add_medio_de_pago() {
                ?>
                    <div class="medios-de-pago mb-5">
                        <p class="m-0 d-block">Medios de pago:</p>
                        <img style="max-width:300px" class="img-fluid d-block w-100" 
                            src="<?php echo get_template_directory_uri() . '/assets/img/webpay-horizontal.png)' ;?>">
                    </div>
                <?php
            }

            // Mostrar productos relacionados solo si no existen ventas cruzadas
            remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
            remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
            add_action( 'woocommerce_after_single_product_summary', 'senzofit_ventas_cruzadas_productos', 15 );

            function senzofit_ventas_cruzadas_productos() {
                global $product;

                if ( isset( $product ) && is_product() ) {
                    $upsells = version_compare( WC_VERSION, '3.0', '<' ) ? $product->get_upsells() : $product->get_upsell_ids();

                    if ( count( $upsells ) > 0 ) {
                        woocommerce_upsell_display();	
                    } else {
                        woocommerce_upsell_display();
                        woocommerce_output_related_products();
                    }
                }
            }
        }


    /**
    * Modificaciones pagina tienda.
    */
        if(is_shop() || is_product_category() ) {

            // Añadir contenedor pagina tienda
            add_action( 'woocommerce_before_main_content', 'senzofit_shop_wrapper_open', 1);
            function senzofit_shop_wrapper_open() {
                echo '<div class="pagina-tienda">';
            }
            add_action( 'woocommerce_after_main_content', 'senzofit_shop_wrapper_close', 12);
            function senzofit_shop_wrapper_close() {
                echo '</div>';
            }

            // Remover breadcrumbs y titulo
            remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
            add_filter( 'woocommerce_show_page_title','__return_false' );
            
            // Añadir banner de tienda, titulo y breadcrumbs
            add_action( 'woocommerce_before_main_content', 'senzofit_before_container_content', 3);
            function senzofit_before_container_content() {
                ?>
                    <div style=" background-image:url('<?php echo get_template_directory_uri(). '/assets/img/footer-bg.jpg)' ;?>" class="pagina-tienda__banner"> 
                        <div class="container"> 
                            <h1><?php woocommerce_page_title();?></h1> 
                        </div>
                    </div>
                    <div class="pagina-tienda__breadcrumb container">
                        <?php woocommerce_breadcrumb(); ?>
                    </div>
                <?php
            }

            // Añadir modal filtros movil
            add_action( 'woocommerce_after_main_content', 'senzofit_modal_filtros', 13);
            function senzofit_modal_filtros() {
                ?>
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="exampleModalLabel">Filtros</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="pagina-tienda__sidebar-modal">
                                    <?php dynamic_sidebar( 'sidebar-2' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                <?php
            }

            // Añadir btn trigger modal filtros movil
            add_action('woocommerce_after_main_content', 'senzofit_btn_filtros_movil');
            function senzofit_btn_filtros_movil() {
                ?>
                    <button type="button" class="btn-filtro-movil d-block d-lg-none" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        <i class='bx bx-filter-alt' ></i>
                    </button>
                <?php
            }

            // Añadir filtros escritorio
            add_action( 'woocommerce_before_main_content', 'woocommerce_get_sidebar' ,7);

            // cierre y apertura  contenedor filtros
            add_action( 'woocommerce_before_main_content', 'senzofit_add_sidebar_tags', 6);
            function senzofit_add_sidebar_tags() {
                echo '<div class="col-lg-3 pagina-tienda__sidebar d-none d-lg-block">';
            }
            add_action( 'woocommerce_before_main_content', 'senzofit_close_sidebar_tags', 8);
            function senzofit_close_sidebar_tags() {
                echo '</div>';
            }
            
            // cierre y apertura contenedor productos
            add_action( 'woocommerce_before_main_content', 'senzofit_add_shop_tags', 9);
            function senzofit_add_shop_tags() {
                echo '<div class="col-lg-9 pagina-tienda__lista-productos">';
            } 
            add_action( 'woocommerce_before_main_content', 'senzofit_close_shop_tags', 11);
            function senzofit_close_shop_tags() {
                echo '</div>';
            } 
        }


    /**
    * Moficaciones pagina finalizar pago.
    */
        if(is_checkout()) {
            
            // Mover de posicion detalles de la compra
            remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
            add_action( 'woocommerce_checkout_shipping', 'woocommerce_order_review', 12);

            //Mover de posicion pago de la compra
            remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
            add_action( 'woocommerce_checkout_shipping', 'woocommerce_checkout_payment', 13);
        }


    /**
    * Modificaciones pagina carro de compras.
    */
        if(is_cart()) {

            
            // Remover ventas cruzadas
            remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
        
            // Añadir row tabla y total carro
            add_action('woocommerce_before_cart', 'senzofit_open_row');
            function senzofit_open_row() {
                echo '<section class="row">';
            }
            add_action('woocommerce_after_cart', 'senzofit_close_row', 2);
            function senzofit_close_row() {
                echo '</section">';
                ?>
            <?php
            }

            // Añadir columna tabla
            add_action('woocommerce_before_cart', 'senzofit_open_col_table');
            function senzofit_open_col_table() {
                echo '<div class="col-xl-8">';
            }
            add_action('woocommerce_after_cart_table', 'senzofit_close_col_table', 1);
            function senzofit_close_col_table() {
                echo '</div>';
            }

            // Añadir columna total carro
            add_action('woocommerce_after_cart_table', 'senzofit_open_col_cart_totals', 2);
            function senzofit_open_col_cart_totals() {
                echo '<div class="col-xl-4">';
            }
            add_action('woocommerce_after_cart', 'senzofit_close_col_cart_totals', 1);
            function senzofit_close_col_cart_totals() {
                echo '</div">';
            }

            // Modicicar pagina carrito vacio
            add_action( 'woocommerce_cart_is_empty', 'senzofit_add_content_empty_cart' );
            function senzofit_add_content_empty_cart() {
                ?>
                    <img class="carrito-vacio__img img-fluid" src="<?php echo get_template_directory_uri() ;?>/assets/img/carrito-vacio.png" />
                <?php
            }
        }

    
    /**
    * Modificación boton cantidad de productos.
    */
        add_action( 'woocommerce_after_quantity_input_field', 'senzofit_display_quantity_plus' );
        function senzofit_display_quantity_plus() {
            echo '<button type="button" class="plus" ><i class="bx bx-plus"></i></button>';
        }
        add_action( 'woocommerce_before_quantity_input_field', 'senzofit_display_quantity_minus' );
        function senzofit_display_quantity_minus() {
            echo '<button type="button" class="minus" ><i class="bx bx-minus"></i></button>';
        }

        add_action( 'wp_footer', 'senzofit_add_cart_quantity_plus_minus' );
        function senzofit_add_cart_quantity_plus_minus() {
        
            if ( ! is_product() && ! is_cart() ) return;
                
            wc_enqueue_js( "   
                    
            $(document).on('click','button.plus, button.minus',function(e){
            
                    var qty = $( this ).parent( '.quantity' ).find( '.qty' );
                    var val = parseFloat(qty.val());
                    var max = parseFloat(qty.attr( 'max' ));
                    var min = parseFloat(qty.attr( 'min' ));
                    var step = parseFloat(qty.attr( 'step' ));

                    $('.woocommerce .actions button.button').prop('disabled', false);
            
                    if ( $( this ).is( '.plus' ) ) {
                        if ( max && ( max <= val ) ) {
                        qty.val( max );
                        } else {
                        qty.val( val + step );
                        }
                    } else {
                        if ( min && ( min >= val ) ) {
                        qty.val( min );
                        } else if ( val > 1 ) {
                        qty.val( val - step );
                        }
                    }
            
                });
                    
            " );
        }

}

add_action( 'wp', 'senzofit_wc_modifications' );


