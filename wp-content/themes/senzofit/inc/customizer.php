<?php
/**
 * senzofit Theme Customizer
 *
 * @package senzofit
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function senzofit_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'senzofit_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'senzofit_customize_partial_blogdescription',
			)
		);
	}

	$button_image_settings = array(
		'select' => 'Seleccionar imagen',
		'remove' => 'Eliminar imagen',
		'change' => 'Cambiar imagen',
		'default' => 'Default',
		'placeholder' => 'Ninguna imagen seleccionada',
		'frame_title' => 'Seleccionar imagen',
		'frame_button' => 'Seleccionar',
	);


	/**
	  *  Customizer seccion productos destacado Inicio
	*/
	$wp_customize->add_section(
		'sec_producto_destacado', array(
			'title' => 'Seccion producto destacado Inicio',
			'description' => 'Configuar producto destacado'
		)
	);

	// Titulo seccion producto destacado
	$wp_customize->add_setting(  
		'set_producto_destacado_titulo', array( 
			'type' => 'theme_mod',
			'default' => 'CALIDAD PREMIUM',
			'sanitize_callback' => 'sanitize_text_field'
		)
	);
	$wp_customize->add_control( 
		'set_producto_destacado_titulo', array(
			'label' => 'Titulo',
			'type' => 'text',
			'section' => 'sec_producto_destacado',
		)
	);


	// Texto seccion producto destacado
	$wp_customize->add_setting(  
		'set_producto_destacado_texto', array( 
			'type' => 'theme_mod',
			'default' => 'Texto sección producto destacado.',
			'sanitize_callback' => 'sanitize_text_field'
		)
	);
	$wp_customize->add_control( 
		'set_producto_destacado_texto', array(
			'label' => 'Texto',
			'type' => 'textarea',
			'section' => 'sec_producto_destacado',
		)
	);


	// Imagen izquierda seccion producto destacado
	$wp_customize->add_setting(  
		'set_producto_destacado', array( 
			'type' => 'theme_mod',
			'default' => '',
			'sanitize_callback' => 'esc_url_raw'
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'set_producto_destacado', array(
		'label' => 'Imagen izquierda',
		'priority' => 20,
		'section' => 'sec_producto_destacado',
		'settings' => 'set_producto_destacado',
		'button_labels' => $button_image_settings,
	)));


	// Imagen  derecha seccion producto destacado
	$wp_customize->add_setting(  
		'set_producto_destacado_imagen_derecha', array( 
			'type' => 'theme_mod',
			'default' => '',
			'sanitize_callback' => 'esc_url_raw'
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'set_producto_destacado_imagen_derecha', array(
		'label' => 'Imagen derecha',
		'priority' => 20,
		'section' => 'sec_producto_destacado',
		'settings' => 'set_producto_destacado_imagen_derecha',
		'button_labels' => $button_image_settings,
	)));


	/**
	  *  Customizer seccion categorias productos Incio
	*/
	$wp_customize->add_section (
		'sec_categorias_productos', array(
			'title' => 'Sección categorias productos Inicio',
			'description' => 'Cambiar imagenes categorias productos inicio'
		)
	);


	// Imagen cateogria musculacion	
	$wp_customize->add_setting(  
		'set_categorias_productos_musculacion', array( 
			'type' => 'theme_mod',
			'default' => '',
			'sanitize_callback' => 'esc_url_raw'
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'set_categorias_productos_musculacion', array(
		'label' => 'Musculación',
		'priority' => 20,
		'section' => 'sec_categorias_productos',
		'settings' => 'set_categorias_productos_musculacion',
		'button_labels' => $button_image_settings
	)));


	// Imagen categoria fitness
	$wp_customize->add_setting(  
		'set_categorias_productos_fitness', array( 
			'type' => 'theme_mod',
			'default' => '',
			'sanitize_callback' => 'esc_url_raw'
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'set_categorias_productos_fitness', array(
		'label' => 'Fitness',
		'priority' => 20,
		'section' => 'sec_categorias_productos',
		'settings' => 'set_categorias_productos_fitness',
		'button_labels' => $button_image_settings
	)));


	// Imagen categoria Home gym
	$wp_customize->add_setting(  
		'set_categorias_productos_home-gym', array( 
			'type' => 'theme_mod',
			'default' => '',
			'sanitize_callback' => 'esc_url_raw'
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'set_categorias_productos_home-gym', array(
		'label' => 'Home GYM',
		'priority' => 20,
		'section' => 'sec_categorias_productos',
		'settings' => 'set_categorias_productos_home-gym',
		'button_labels' => $button_image_settings
	)));


	// Imagen categoria accesorios
	$wp_customize->add_setting(  
		'set_categorias_productos_accesorios', array( 
			'type' => 'theme_mod',
			'default' => '',
			'sanitize_callback' => 'esc_url_raw'
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'set_categorias_productos_accesorios', array(
		'label' => 'Accesorios',
		'priority' => 20,
		'section' => 'sec_categorias_productos',
		'settings' => 'set_categorias_productos_accesorios',
		'button_labels' => $button_image_settings
	)));
}
add_action( 'customize_register', 'senzofit_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function senzofit_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function senzofit_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function senzofit_customize_preview_js() {
	wp_enqueue_script( 'senzofit-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'senzofit_customize_preview_js' );
