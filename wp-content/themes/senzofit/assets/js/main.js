
/**
 * Slider Categorias Inicio.
 */
var swiper = new Swiper('.slider-categorias', {

	pagination: {
		el: '.categorias-paginacion',
		clickable: true,
	},
	loop: true,
	autoplay: {
		delay: 3000,
		disableOnInteraction: true,
	},
	spaceBetween: 30,
	speed: 1000,
	breakpoints: {
		0: {
			slidesPerView: 2,
			spaceBetween: 16,
			simulateTouch: true,
		},
		770: {
			slidesPerView: 2,
		},
		1000: {
			slidesPerView: 3,
		},
		1190: {
			slidesPerView: 4,
			simulateTouch: false,
			pagination: false,
			loop: false,
		}
	}
});