��          �      l      �  �   �     �     �     �     �     �  <   �  @     
   U     `     v          �     �  
   �  �   �  
   J     U  /   h     �  L  �  �   �     �  !   �     �            >   ,  N   k     �     �     �     �     �       
   (  �   3  
   �     �  *   �     	        	                                                              
                            All you need to do now is select the “Tidio Chat” tab on the left - that will take you to your Tidio panel. You can also open the panel by using the link below. Choose your project Email Address Forgot password? Go to Panel Go to Tidio panel Increase sales by skyrocketing communication with customers. Join 300 000+ websites using Tidio - Live Chat boosted with Bots Let’s go Log into your account Password Pick one from the list Start all over again Start using Tidio Tidio Chat Tidio Live Chat - live chat boosted with chatbots for your online business. Integrates with your website in less than 20 seconds. Tidio Ltd. Type your password Your site is already integrated with Tidio Chat http://www.tidio.com PO-Revision-Date: 2021-09-30 11:04:55+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: es
Project-Id-Version: Plugins - Tidio – Live Chat, Chatbots &amp; Email Marketing - Stable (latest release)
 Ahora, todo lo que tienes que hacer es seleccionar a la izquierda la pestaña «Tidio Chat», que te llevará a tu panel de Tidio. También puedes abrir el panel usando el siguiente enlace. Elige tu proyecto Dirección de correo electrónico ¿Has olvidado la contraseña? Ir al panel Ir al panel de Tidio Aumenta las ventas lanzando la comunicación con los clientes. Únete a más de 300.000 web que usan Tidio - Chat en vivo potenciado con bots Adelante Accede a tu cuenta Contraseña Selecciona uno de la lista Empezar todo de nuevo Empezar a usar Tidio Tidio Chat Chat en vivo de Tidio - Chat en vivo potenciado con chatbots para tu negocio online. Se integra con tu web en menos de 20 segundos. Tidio Ltd. Escribe tu contraseña Tu sitio ya está integrado con Tidio Chat https://www.tidio.com 