<?php
  class Rate {
    public $email = '';
    public $token = '';
    public $cart = '';
    public $multiCourierEnabled = false;
    public $parcel = array();
    public $destinyId = 0;
	  public $success_responses_code = array(200,201,202,203,204);

    public function __construct($email, $token) {
      $this->base = 'http://api.shipit.cl/v';
      $this->email = $email;
      $this->token = $token;
      $this->headers = array(
        'Accept' => 'application/vnd.shipit.v4',
        'Content-Type' => 'application/json',
        'X-Shipit-Email' => $email,
        'X-Shipit-Access-Token' => $token
      );
    }

    function calculate() {
      return $this->getMultiCourierEnabled() ? $this->rates() : $this->prices();
    }

    function prices() {
      $client = new HttpClient($this->base . '/rates', $this->headers);
	    global $wpdb;
      $response = $client->post($this->getParcel());
      $prices = array();
	    if (!in_array(wp_remote_retrieve_response_code($response), $this->success_responses_code)) {
        if (!isset($response->errors)) {
          $jsonBody = json_decode($response['body']);
          if (isset($jsonBody->debtor)) return $prices = '';
        }
        $emergencyRate = new EmergencyRate();
        $rates = $emergencyRate->getEmergencyRate($wpdb, $this->getDestinyId());
        $prices = json_decode($rates)->prices;
        echo 'Error al conectar con API.';
      } else {
        $prices = json_decode($response['body'])->prices;
      }
      return $prices;
    }

    function rates() {
      global $wpdb;
      $client = new HttpClient($this->base . '/rates', $this->headers);
      $response = $client->post($this->getParcel());
      $prices = array();
      $data = array();
      if (!in_array(wp_remote_retrieve_response_code($response), $this->success_responses_code)) {
        if (!isset($response->errors)) {
          $jsonBody = json_decode($response['body']);
          if (isset($jsonBody->debtor)) return $prices = '';
        }
        $emergencyRate = new EmergencyRate();
        $rates = $emergencyRate->getEmergencyRate($wpdb, $this->getDestinyId());
        $prices = json_decode($rates)->prices;
        echo 'Error al conectar con API.';
      } else {
        $prices = json_decode($response['body'])->prices;
        $couriers = array_unique(array_map(function($price) {
          return $price->original_courier;
        }, $prices));
        for ($index = 0; $index < count($couriers); $index++) {
          foreach ($prices as $price) {
            if ($price->original_courier == $couriers[$index]) {
              $couriers = array_slice($couriers, $index + 1);
              $data[] = $price;
              if (count($couriers) === 0) break;
            }
          }
        }
      }
      return $data;
    }

    function getDestinyId() {
      return $this->destinyId;
    }

    function setDestinyId($destinyId) {
      $this->destinyId = $destinyId;
    }

    function getMultiCourierEnabled() {
      return $this->multiCourierEnabled;
    }

    function setMultiCourierEnabled($multiCourierEnabled = false) {
      $this->multiCourierEnabled = $multiCourierEnabled;
    }


    function getParcel() {
      return array(
        'parcel' => array(
          'height' => $this->parcel['height'],
          'width' => $this->parcel['width'],
          'length' => $this->parcel['length'],
          'weight' => $this->parcel['weight'],
          'origin_id' => 308,
          'destiny_id' => $this->getDestinyId(),
          'type_of_destiny' => 'domicilio',
          'rate_from' => 'woocommerce',
          'from_cart' => $this->cart
        )
      );
    }

    function setParcel($parcel = array(), $cart = false) {
      $this->parcel = $parcel;
      $this->cart = $cart;
    }

    function getRateDescription($checkout, $description, $woocommerceSetting) {
      if ($checkout->show_days && $woocommerceSetting['time_despach'] == 'yes') {
        return array($description);
      } else {
        return array(" ");
      }
    }

    function getFreeShipment($freeShipmentByTotalOrderPrice, $woocommerceSetting) {
      $freeDestinies = $woocommerceSetting['free_communes'];
      $freeDestiniesByPrice = $woocommerceSetting['free_communes_for_price'];
      // RETURN NON FREE PRICE IF FREE DESTINIES OR FREE DESTINIES BY PRICES IS EMPTY
      if ($freeDestinies == '' && $freeDestiniesByPrice == '') return false;
      // RETURN FREE SHIPMENT PRICE BASED ON SPECIFIC COMMUNES OR TOTAL CART PRICE OR SPECIFIC COMMUNES WITH PRICE
      if ($freeDestinies != '') {
        return in_array('CL'.strval($this->getDestinyId()), $freeDestinies, TRUE);
      } elseif ($freeDestiniesByPrice != '' && $freeShipmentByTotalOrderPrice == true) {
        return in_array('CL'.strval($this->getDestinyId()), $freeDestiniesByPrice, TRUE);
      } else {
        return false;
      }
    }

    function getRate($id, $price, $carrierName, $rateDescription, $specificDestinyPrice, $freeShipment, $woocommerceSetting) {
      $rate = $price->price;
      if ($freeShipment) {
        $rate = 0;
      } elseif ($woocommerceSetting['active-setup-price'] == 'yes') {
        if ($specificDestinyPrice == true) {
          $rate = $price->price - (($price->price * $woocommerceSetting['price-setup']) / 100);
        }
      }

      return array(
        'id'    => $id,
        'label' => $carrierName,
        'cost'  => $rate,
        'meta_data' => $rateDescription
      );
    }
  }
?>
